const graphiql = require('graphql')
const Image = require('../type/image_object')
const { Create } = require('../functions/image')

const {
  GraphQLString
} = graphiql


const CreateImage = {
  type: Image,
  args: {
    image: {type: GraphQLString}
  }, 
  resolve: async (_, args) => {
    console.log('args image', args)
    var resCreateImage = await Create(args)
    return resCreateImage
  }
}
module.exports = { CreateImage }