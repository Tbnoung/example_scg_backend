const graphiql = require('graphql')
const Mail  = require('../type/mail')
const { GraphQLString } = graphiql
const { SentMailTest } = require('../functions/mail')

const SendMail = {
  type: Mail,
  args: {
    discription: {type: GraphQLString},
    location: {type: GraphQLString},
    email: {type: GraphQLString},
    product_name: {type: GraphQLString},
    qty: {type: GraphQLString},
    price: {type: GraphQLString}
  }, 
  resolve: async (_, args) => {
    var result = await SentMailTest(args)
    return result
  }
}

module.exports = { SendMail }