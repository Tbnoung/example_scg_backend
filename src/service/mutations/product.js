const graphiql = require('graphql')
const ProductObject = require('../type/product_object')
const { Middleware } = require('../functions/checktoken')
const { Create, UpdateProductByID } = require('../functions/product')
const { GraphQLString } = graphiql
const CreateProduct = {
  type: ProductObject,
  args: {
    token: {type: GraphQLString},
    product_name: {type: GraphQLString},
    price: {type: GraphQLString},
    qty: {type: GraphQLString},
    image: {type: GraphQLString}
  }, 
  resolve: async (_, args) => {
    var check = await Middleware(args.token)
    args.create_by = check.data.firstname
    if (check !== '') {
      var Result = await Create(args)
      return Result
    } else {
      var res = await response('500', '', 'Unauthenlize', 'FAIL')
      return res
    }
  }
}
const UpdateProduct = {
  type: ProductObject,
  args: {
    id: {type: GraphQLString},
    product_name: {type: GraphQLString},
    price: {type: GraphQLString},
    qty: {type: GraphQLString},
    image: {type: GraphQLString}
  }, 
  resolve: async (_, args) => {
    var Result = await UpdateProductByID(args)
    return Result
  }
}


module.exports = { CreateProduct, UpdateProduct }
