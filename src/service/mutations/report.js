const ReportObject = require('../type/report_object')
const { Create } = require('../functions/report')
const graphiql = require('graphql')
const {
  GraphQLString
} = graphiql

const CreateReport = {
  type: ReportObject,
  args: {
    id_machine: {type: GraphQLString},
    id_product: {type: GraphQLString},
    location: {type: GraphQLString},
    discription: {type: GraphQLString},
    product_name: {type: GraphQLString},
    qty: {type: GraphQLString},
    price: {type: GraphQLString}
  },
  resolve: async (_, args) => {
    var Result = await Create(args)
    return Result
  }
}
module.exports = { CreateReport }