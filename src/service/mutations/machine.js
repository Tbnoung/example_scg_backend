const graphiql = require('graphql')
const MachineObject = require('../type/machine_object')
const { Middleware } = require('../functions/checktoken')
const { Create, UpdateMachineByID, UpdateMachineProducts } = require('../functions/machine')
const { GraphQLString } = graphiql

const CreateMachine = {
  type: MachineObject,
  args: {
    token: {type: GraphQLString},
    location: {type: GraphQLString},
    discription: {type: GraphQLString},
    slot: {type: GraphQLString},
    update_by: {type: GraphQLString},
    email: {type: GraphQLString},
    machine_product: {type: GraphQLString}
  }, 
  resolve: async (_, args) => {
    var check = await Middleware(args.token)
    args.create_by = check.data.firstname
    if (check !== '') {
      var Result = await Create(args)
      return Result
    } else {
      var res = await response('500', '', 'Unauthenlize', 'FAIL')
      return res
    }
  }
}
const UpdateMachine = {
  type: MachineObject,
  args: {
    id: {type: GraphQLString},
    location: {type: GraphQLString},
    discription: {type: GraphQLString},
    email: {type: GraphQLString}
    // slot: {type: GraphQLString},
    // update_by: {type: GraphQLString},
    // machine_product: {type: GraphQLString}
  }, 
  resolve: async (_, args) => {
    var result = await UpdateMachineByID(args)
    return result
  }
}

const UpdateMachineProduct = {
  type: MachineObject,
  args: {
    id_machine: {type: GraphQLString},
    id_product: {type: GraphQLString},
    qty: {type: GraphQLString},
    data: {type: GraphQLString}
  }, 
  resolve: async (_, args) => {
    var result = await UpdateMachineProducts(args)
    return result
  }
}



module.exports = { CreateMachine, UpdateMachine, UpdateMachineProduct }
