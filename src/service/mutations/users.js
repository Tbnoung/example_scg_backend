const UserObject = require('../type/user_object')
const graphiql = require('graphql')
const { CreateUser, UpdateUserByID } = require('../functions/users')
const {
  GraphQLString
} = graphiql

const Register = {
  type: UserObject,
  args: {
    username: {type: GraphQLString},
    password: {type: GraphQLString},
    firstname: {type: GraphQLString},
    lastname: {type: GraphQLString},
    role: {type: GraphQLString}
  }, 
  resolve: async (_, args) => {
    var Result = await CreateUser(args)
    return Result
  }
}
const UpdateUser = {
  type: UserObject,
  args: {
    id: {type: GraphQLString},
    username: {type: GraphQLString},
    firstname: {type: GraphQLString},
    lastname: {type: GraphQLString}
  }, 
  resolve: async (_, args) => {
    var Result = await UpdateUserByID(args)
    return Result
  }
}

module.exports = { Register, UpdateUser }
