const graphiql = require('graphql')
const {
  GraphQLObjectType,
  GraphQLString
} = graphiql

const Image = new GraphQLObjectType({
  name: 'Image',
  description: 'Document for Image...',
  fields: () => ({
    code: {type: GraphQLString},
    data: {type: GraphQLString},
    message: {type: GraphQLString},
    result: {type: GraphQLString}
  })
})

module.exports = Image