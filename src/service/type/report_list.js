const graphiql = require('graphql')
const {
  GraphQLObjectType,
  GraphQLString,
  GraphQLList
} = graphiql

const ReportList = new GraphQLObjectType({
  name: 'ReportList',
  description: 'Document for ReportList...',
  fields: () => ({
    code: {type: GraphQLString},
    data: {type: new GraphQLList(ReportListType)},
    message: {type: GraphQLString},
    result: {type: GraphQLString}
  })
})
const ReportListType = new GraphQLObjectType({
  name: 'ReportListType',
  description: 'Document for ReportListType...',
  fields: () => ({
    id_machine: {type: GraphQLString},
    id_product: {type: GraphQLString},
    location: {type: GraphQLString},
    discription: {type: GraphQLString},
    product_name: {type: GraphQLString},
    qty: {type: GraphQLString},
    price: {type: GraphQLString}
  })
})

module.exports = ReportList