const graphiql = require('graphql')
const {
  GraphQLObjectType,
  GraphQLString,
  GraphQLList
} = graphiql

const MachineList = new GraphQLObjectType({
  name: 'MachineList',
  description: 'Document for MachineList...',
  fields: () => ({
    code: {type: GraphQLString},
    data: {type: new GraphQLList(MachineListType)},
    message: {type: GraphQLString},
    result: {type: GraphQLString}
  })
})
const MachineListType = new GraphQLObjectType({
  name: 'MachineListType',
  description: 'Document for MachineListType...',
  fields: () => ({
    id: {type: GraphQLString},
    id_user: {type: GraphQLString},
    username: {type: GraphQLString},
    email: {type: GraphQLString},
    location: {type: GraphQLString},
    discription: {type: GraphQLString},
    slot: {type: GraphQLString},
    update_by: {type: GraphQLString},
    create_by: {type: GraphQLString},
    machine_product: {type: GraphQLString}
  })
})

module.exports = MachineList