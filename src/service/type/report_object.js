const graphiql = require('graphql')
const {
  GraphQLObjectType,
  GraphQLString
} = graphiql

const ReportObject = new GraphQLObjectType({
  name: 'ReportObject',
  description: 'Document for ReportObject...',
  fields: () => ({
    code: {type: GraphQLString},
    data: {type: ReportObjectType},
    message: {type: GraphQLString},
    result: {type: GraphQLString}
  })
})
const ReportObjectType = new GraphQLObjectType({
  name: 'ReportObjectType',
  description: 'Document for ReportObjectType...',
  fields: () => ({
    id_machine: {type: GraphQLString},
    id_product: {type: GraphQLString},
    location: {type: GraphQLString},
    discription: {type: GraphQLString},
    product_name: {type: GraphQLString},
    qty: {type: GraphQLString},
    price: {type: GraphQLString}
  })
})

module.exports = ReportObject