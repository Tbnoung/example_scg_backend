const graphiql = require('graphql')
const {
  GraphQLObjectType,
  GraphQLString,
  GraphQLList
} = graphiql

const UserList = new GraphQLObjectType({
  name: 'UserList',
  description: 'Document for UserList...',
  fields: () => ({
    code: {type: GraphQLString},
    data: {type: new GraphQLList(UserListType)},
    message: {type: GraphQLString},
    result: {type: GraphQLString}
  })
})
const UserListType = new GraphQLObjectType({
  name: 'UserListType',
  description: 'Document for UserListType...',
  fields: () => ({
    id: {type: GraphQLString},
    username: {type: GraphQLString},
    password: {type: GraphQLString},
    firstname: {type: GraphQLString},
    lastname: {type: GraphQLString},
    role: {type: GraphQLString}
  })
})

module.exports = UserList