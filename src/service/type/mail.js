const graphiql = require('graphql')
const {
  GraphQLObjectType,
  GraphQLString
} = graphiql

const Mail = new GraphQLObjectType({
  name: 'Mail',
  description: 'Document for Mail...',
  fields: () => ({
    code: {type: GraphQLString},
    data: {type: GraphQLString},
    message: {type: GraphQLString},
    result: {type: GraphQLString}
  })
})

module.exports = Mail