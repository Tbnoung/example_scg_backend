const graphiql = require('graphql')
const {
  GraphQLObjectType,
  GraphQLString,
  GraphQLList
} = graphiql

const ProductList = new GraphQLObjectType({
  name: 'ProductList',
  description: 'Document for ProductList...',
  fields: () => ({
    code: {type: GraphQLString},
    data: {type: new GraphQLList(ProductListType)},
    message: {type: GraphQLString},
    result: {type: GraphQLString}
  })
})
const ProductListType = new GraphQLObjectType({
  name: 'ProductListType',
  description: 'Document for ProductListType...',
  fields: () => ({
    id: {type: GraphQLString},
    product_name: {type: GraphQLString},
    price: {type: GraphQLString},
    qty: {type: GraphQLString},
    image: {type: GraphQLString},
  })
})


module.exports = ProductList