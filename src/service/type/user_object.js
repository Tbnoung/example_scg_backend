const graphiql = require('graphql')
const {
  GraphQLObjectType,
  GraphQLString
} = graphiql

const UserObject = new GraphQLObjectType({
  name: 'UserObject',
  description: 'Document for UserObject...',
  fields: () => ({
    code: {type: GraphQLString},
    data: {type: UserObjectType},
    message: {type: GraphQLString},
    result: {type: GraphQLString}
  })
})
const UserObjectType = new GraphQLObjectType({
  name: 'UserObjectType',
  description: 'Document for UserObjectType...',
  fields: () => ({
    id: {type: GraphQLString},
    username: {type: GraphQLString},
    password: {type: GraphQLString},
    firstname: {type: GraphQLString},
    lastname: {type: GraphQLString},
    role: {type: GraphQLString},
    token: {type: GraphQLString}
  })
})

module.exports = UserObject