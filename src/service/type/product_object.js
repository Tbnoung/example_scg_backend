const graphiql = require('graphql')
const {
  GraphQLObjectType,
  GraphQLString
} = graphiql

const ProductObject = new GraphQLObjectType({
  name: 'ProductObject',
  description: 'Document for ProductObject...',
  fields: () => ({
    code: {type: GraphQLString},
    data: {type: ProductObjectType},
    message: {type: GraphQLString},
    result: {type: GraphQLString}
  })
})
const ProductObjectType = new GraphQLObjectType({
  name: 'ProductObjectType',
  description: 'Document for ProductObjectType...',
  fields: () => ({
    id: {type: GraphQLString},
    product_name: {type: GraphQLString},
    price: {type: GraphQLString},
    qty: {type: GraphQLString},
    image: {type: GraphQLString},
  })
})


module.exports = ProductObject