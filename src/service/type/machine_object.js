const graphiql = require('graphql')
const {
  GraphQLObjectType,
  GraphQLString
} = graphiql

const MachineObject = new GraphQLObjectType({
  name: 'MachineObject',
  description: 'Document for MachineObject...',
  fields: () => ({
    code: {type: GraphQLString},
    data: {type: MachineObjectType},
    message: {type: GraphQLString},
    result: {type: GraphQLString}
  })
})
const MachineObjectType = new GraphQLObjectType({
  name: 'MachineObjectType',
  description: 'Document for MachineObjectType...',
  fields: () => ({
    id: {type: GraphQLString},
    id_user: {type: GraphQLString},
    username: {type: GraphQLString},
    email: {type: GraphQLString},
    location: {type: GraphQLString},
    discription: {type: GraphQLString},
    slot: {type: GraphQLString},
    update_by: {type: GraphQLString},
    create_by: {type: GraphQLString},
    machine_product: {type: GraphQLString}
  })
})

module.exports = MachineObject