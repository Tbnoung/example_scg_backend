const graphiql = require('graphql')
const { Register, UpdateUser } = require('../mutations/users')
const { CreateMachine, UpdateMachine, UpdateMachineProduct } = require('../mutations/machine')
const { CreateProduct, UpdateProduct } = require('../mutations/product')
const { CreateReport } = require('../mutations/report')
const { SendMail } = require('../mutations/mail')
const { CreateImage } = require('../mutations/image')
const {
  GraphQLObjectType
} = graphiql

const Mutations = new GraphQLObjectType({
  name: 'Mutation',
  description: 'Description Mutation',
  fields: {
    Register: Register,
    UpdateUser: UpdateUser,
    CreateMachine: CreateMachine,
    UpdateMachine: UpdateMachine,
    CreateProduct: CreateProduct,
    UpdateProduct: UpdateProduct,
    UpdateMachineProduct: UpdateMachineProduct,
    CreateReport: CreateReport,
    SendMail: SendMail,
    CreateImage: CreateImage
  }
})

module.exports = Mutations