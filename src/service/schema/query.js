const graphiql = require('graphql')
const { GetAllUser, GetUserByID } = require('../querys/users')
const { GetAllMachine, GetMachineByID, GetMachineByIDUser } = require('../querys/machine')
const { GetAllProduct } = require('../querys/product')
const { Login } = require('../querys/login')
const { GetAllReport } = require('../querys/report')
const { GraphQLObjectType } = graphiql
const RootQuery = new GraphQLObjectType ({
  name: 'RootQueryType',
  description: 'Description RootQueryType',
  fields: {
    GetAllUser: GetAllUser,
    GetUserByID: GetUserByID,
    Login: Login,
    GetAllMachine: GetAllMachine,
    GetMachineByID: GetMachineByID,
    GetAllProduct: GetAllProduct,
    GetMachineByIDUser: GetMachineByIDUser,
    GetAllReport: GetAllReport
  }
})

module.exports = RootQuery