const Schema_user = require('../../db/schema/schema_user')
const { response } = require('../functions/response')
const bcrypt = require('bcrypt')
// ------------------------------Create User---------------------------
const CreateUser = async (val) => {
  try {
    var ResultCheckUser =  await CheckUsername(val.username)
    if (ResultCheckUser) {
      val.password = await bcrypt.hash(val.password, 8)
      await CreateUsers(val)
      var res = await response('200', '', 'Create user success', 'SUCCESS')
      return res
    } else {
      var res = await response('200', '', 'Duplicate username', 'FAIL')
      return res
    }
  } catch (error) {
    var res = await response('500', '', 'Server error', 'FAIL')
    return res
  }
}
const CreateUsers = async (val) => {
  let create = new Schema_user(val)
  await create.save()
  return 'SUCCESS'
}
// ------------------------------Check User---------------------------
const CheckUsername = async (username) => {
  try {
    var check = await Schema_user.findOne({username: username})
    if (check !== null) {
      return false
    } else {
      return true
    }
  } catch (error) {
    return false
  }
}
// ------------------------------Get All User---------------------------
const GetAllUsers = async () => {
  try {
    var GetData = await Schema_user.find({})
    if (GetData.length !== 0) {
      const SetDataUser = GetData.filter((data) => {
        return data.role !== 'machine'
      })

      var res = await response('200', SetDataUser, 'Get user success', 'SUCCESS')
      return res
    } else {
      var res = await response('200', [], 'No user', 'Fail')
      return res
    }
  } catch (error) {
    var res = await response('500', '', 'Server error', 'FAIL')
    return res
  }
}

// ------------------------------Get User By ID ---------------------------

const GetUserID = async (val) => {
  try {
    var GetData = await Schema_user.findOne({_id:val})
    if ( GetData.length !== null) {
      var res = await response('200', GetData, 'Get user success', 'SUCCESS')
      return res
    } else {
      var res = await response('200', [], 'No user', 'Fail')
      return res
    }
  } catch (error) {
    var res = await response('500', '', 'Server error', 'FAIL')
    return res
  }
}

// ------------------------------Edit user ---------------------------

const UpdateUserByID = async (val) => {
  try {
    var filler = { _id: val.id}
    var dataupdate = {
      username: val.username,
      firstname: val.firstname,
      lastname: val.lastname
    }
    var update = await Schema_user.findByIdAndUpdate(filler, dataupdate)
    if (update !== null) {
      var res = await response('200', '', 'Update user success', 'SUCCESS')
      return res
    } else {
      var res = await response('200', '', 'User not found', 'FAIL')
      return res
    }
  } catch (error) {
    var res = await response('500', '', 'Server error', 'FAIL')
    return res
  }
}



module.exports = { CreateUser, GetAllUsers, GetUserID, UpdateUserByID }