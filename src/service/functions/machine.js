const Schema_machine = require('../../db/schema/schema_machine')
const Schema_product = require('../../db/schema/schema_product')
const Schema_user = require('../../db/schema/schema_user')
const bcrypt = require('bcrypt')
const { response } = require('./response')

const Create = async(val) => {
  try {
    const Count = await CountMachine()
    await CreateUserMachine(Count)
    const data = await GetIDMachine(Count)
    val.id_user = data
    val.username = `machine${Count + 1}`
    console.log(val)
    const CreateMachines = await CreateMachine(val)
    if (CreateMachines === 'SUCCESS') {
      var res =  response('200', '', 'Create machine success', 'SUCCESS')
      return res
    } else {
      var res = response('200', '', 'Create machine fail', 'FAIL')
      return res
    }
  } catch (error) {
    var res = response('500', '', 'Server error', 'FAIL')
    return res
  }
}

const CountMachine = async() => {
  var count = await Schema_machine.find({})
  return count.length
}
const CreateUserMachine = async(val) => {
  var data = {
    username: `machine${val + 1}`,
    password: await bcrypt.hash('1234', 8),
    role: 'machine', 
    firstname: '',
    lastname: ''
  }
  let create = new Schema_user(data)
  await create.save()
  return ''
}
const GetIDMachine = async (val) => {
  var data = await Schema_user.findOne({username: `machine${val + 1}`})
  return data._id
}
const CreateMachine = async(val) => {
  let create = new Schema_machine(val)
  console.log('create', create)
  await create.save()
  return 'SUCCESS'
}

const UpdateMachineByID = async(val) => {
  try {
    var filler = { _id: val.id}
    var dataupdate = {
      location: val.location,
      discription: val.discription,
      email: val.email,
    }
    var update = await Schema_machine.findByIdAndUpdate(filler, dataupdate)
    if (update !== null) {
      var res = response('200', '', 'Update machine success', 'SUCCESS')
      return res
    } else {
      var res = response('200', '', 'Machine not found', 'FAIL')
      return res
    }
  } catch (error) {
    var res = response('500', '', 'Server error', 'FAIL')
    return res
  }
}

const GetAllMachines = async() => {
  try {
    var GetData = await Schema_machine.find({})
    if (GetData.length !== 0) {
      var res = response('200', GetData, 'Get user success', 'SUCCESS')
      return res
    } else {
      var res = response('200', [], 'No user', 'Fail')
      return res
    }
  } catch (error) {
    var res = response('500', '', 'Server error', 'FAIL')
    return res
  }
}

const GetMachineID = async (val) => {
  try {
    var GetData = await Schema_machine.findOne({_id:val.id})
    if ( GetData.length !== null) {
      var res = response('200', GetData, 'Get machine success', 'SUCCESS')
      return res
    } else {
      var res = response('200', [], 'No machine', 'Fail')
      return res
    }
  } catch (error) {
    var res = response('500', '', 'Server error', 'FAIL')
    return res
  }
}
const GetMachineByIDMachine = async (val) => {
  try {
    var GetData = await Schema_machine.findOne({id_user:val.id})
    if ( GetData.length !== null) {
      var res = response('200', GetData, 'Get machine success', 'SUCCESS')
      return res
    } else {
      var res = response('200', [], 'No machine', 'Fail')
      return res
    }
  } catch (error) {
    var res = response('500', '', 'Server error', 'FAIL')
    return res
  }
}

const UpdateMachineProducts = async (val) => {
  try {
    var check = await MinusProduct(val)
    if (check === 'Fail') {
      var res = response('200', '', 'Unable to add products', 'FAIL')
      return res
    } else {
      var filler = { _id: val.id_machine}
      var dataupdate = {
        machine_product: val.data
      }
      var update = await Schema_machine.findByIdAndUpdate(filler, dataupdate)
      if (update !== null) {
        var res = response('200', '', 'Update machine product success', 'SUCCESS')
        return res
      } else {
        var res = response('200', '', 'Machine product  fail', 'FAIL')
        return res
      }
    }
  } catch (error) {
    var res = response('500', '', 'Server error', 'FAIL')
    return res
  }
}
const MinusProduct = async (val) => {
  var getProduct = await Schema_product.findById({_id: val.id_product})
  if (parseInt(getProduct.qty) > parseInt(val.qty)) {
    var minus = parseInt(getProduct.qty) - parseInt(val.qty)
    var filler = { _id: val.id_product }
    var dataupdate = {
      qty: minus
    }
    await Schema_product.findByIdAndUpdate(filler, dataupdate)
    return 'SUCCESS' 
  } else {
    return 'Fail'
  }
}

module.exports = { Create, UpdateMachineByID, GetAllMachines, GetMachineID, UpdateMachineProducts, GetMachineByIDMachine }
