const Schema_report = require('../../db/schema/schema_report')
const { response } = require('./response')
const Create = async (val) => {
  try {
    var check = await Check(val)
    if (!check) {
      await UpdateReport(val)
      var res = response('200', [], 'Successful order', 'SUCCESS')
      return res
    } else {
      await CreateReport(val)
      var res = response('200', [], 'Successful order', 'SUCCESS')
      return res
    }
  } catch (error) {
    var res = response('500', '', 'Server error', 'FAIL')
    return res
  }
}
const Check = async (val) => {
  try {
    var data = {
      // id_machine: val.id_machine,
      id_product: val.id_product
    }
    var check = await Schema_report.findOne(data)
    if (check !== null) {
      return false
    } else {
      return true
    }
  } catch (error) {
    return false
  }
}
const UpdateReport = async(val) => {
  try {
    var data = {
      // id_machine: val.id_machine,
      id_product: val.id_product
    }
    var report = await Schema_report.findOne(data)
    var update = {
      qty: (parseInt(report.qty) + 1).toString()
    }
    await Schema_report.findOneAndUpdate(data, update)
    return true
  } catch (error) {
    return false
  }
}
const CreateReport = async(val) => {
    let create = new Schema_report(val)
    await create.save()
    return 'SUCCESS'
} 
const GetAllReports = async () => {
  try {
    var GetData = await Schema_report.find({})
    if (GetData.length !== 0) {
      var res = response('200', GetData, 'Get report success', 'SUCCESS')
      return res
    } else {
      var res = response('200', [], 'No report', 'Fail')
      return res
    }
  } catch (error) {
    var res = response('500', '', 'Server error', 'FAIL')
    return res
  }
}
module.exports = { Create, GetAllReports }