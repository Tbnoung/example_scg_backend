const nodemailer = require('nodemailer')
const { response } = require('./response')
const SendEMail = async (val) => {
return new Promise((resolve,reject)=>{
  const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: 'testtbnoung@gmail.com',
      pass: 'Buakai001'
    }
  })
  transporter.sendMail(val, function (err, info) {
    if(err)
      resolve(false)
    else
      resolve(true)
    })
  })
}
const SentMailTest = async (val) => {
  try {
    var SetDataSend = SetDataSentMail(val)
    var sendmail = await SendEMail(SetDataSend)
    if (sendmail) {
      var res = response('200', '', 'Send mail success', 'SUCCESS')
      return res
    } else {
      var res = response('200', '', 'Send mail fail', 'FAIL')
      return res
    }
  } catch (error) {
    var res = response('500', '', 'Server error', 'FAIL')
    return res
  }
}
const SetDataSentMail = (val) => {
  const mailOptions = {
    from: 'tbnoung@gmail.com',
    to: val.email,
    subject: 'เเจ้งเตือนสินค้าใกล้หมด',
    html: 
    `
    <div>
      <div>ชื่อสินค้า ${val.product_name}</div>
      <div>สถานที่ ${val.location}</div>
      <div>รายละเอียดสถานที่ตั้งเครื่อง ${val.discription} </div>
      <div>จำนวนคงเหลือ ${val.qty} ชิ้น</div>
    </div>
    `
  };
  return mailOptions
}

module.exports = { SentMailTest }