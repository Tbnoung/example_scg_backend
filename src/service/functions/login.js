  const Schema_user = require('../../db/schema/schema_user')
  const { response } = require('./response')
  const bcrypt = require('bcrypt')
  const jwt = require('jsonwebtoken')
  const Logins = async (val) => {
    try {
        const FindUser = await Schema_user.findOne({username:val.username})
        if (FindUser !== null) {
          const ChackPassword = await bcrypt.compare(val.password, FindUser.password)
          if (ChackPassword) {
            var token  = await jwt.sign({ data:FindUser }, 'Tbnoung', {expiresIn: '365 day'})
            FindUser.token = token
            var res = await response('200', FindUser, 'Login success', 'SUCCESS')
          } else {
            var res = await response('200', '', 'username or password incorrect', 'FAIL')
          }
        } else {
          var res = await response('200', '', 'username or password incorrect', 'FAIL')
        }
        return res
    } catch (error) {
      var res = await response('500', '', 'No username or password', 'FAIL')
      return res
    }
  }
  module.exports = { Logins }