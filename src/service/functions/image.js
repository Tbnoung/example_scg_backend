var fs = require('fs')
const { response } = require('./response')

const Create = async (val) => {
  try {
    var data = await CreateImg(val)
    var res = await response('200', data, 'create image success', 'Success')
    return res
  } catch (error) {
    var res = response('500', '', 'Server error', 'FAIL')
    return res
  }
}

const CreateImg = (val) => {
  try {
    console.log('val check val', val)
    var base64Data = val.image.split(",")[1]
    console.log('val check val', base64Data)
    var dir = `src/img/product`
    if (!fs.existsSync(dir)){
        fs.mkdirSync(dir)
    }
    var date = new Date()
    var now = date.getTime()
    const dataImg = `${dir}/${now}.jpeg`
    console.log('rwhite file', dataImg)
    fs.writeFileSync(dataImg, base64Data, 'base64')
    return `${now}.jpeg`
  } catch (error) {
    return error.response.data
  }
}
module.exports = { Create }