const Schema_product = require('../../db/schema/schema_product')
const { response } = require('./response')
const Create = async(val) => {
  try {
     var CheckProduct =  await CheckName(val.product_name)
     if (CheckProduct) {
      await CreateProduct(val)
      var res = await response('200', '', 'Create product success', 'SUCCESS')
      return res
    } else {
      var res = await response('200', '', 'Duplicate product_name', 'FAIL')
      return res
    }
  } catch (error) {
    var res = await response('500', '', 'Server error', 'FAIL')
    return res
  }
}
const CreateProduct = async(val) => {
  let create = new Schema_product(val)
  await create.save()
  return 'SUCCESS'
}
const CheckName = async(product_name) => {
  try {
    var check = await Schema_product.findOne({product_name: product_name})
    if (check !== null) {
      return false
    } else {
      return true
    }
  } catch (error) {
    return false
  }
}

const GetAllProducts = async() => {
  try {
    var GetData = await Schema_product.find({})
    if (GetData.length !== 0) {
      var res = response('200', GetData, 'Get product success', 'SUCCESS')
      return res
    } else {
      var res = response('200', [], 'No product', 'Fail')
      return res
    }
  } catch (error) {
    var res = response('500', '', 'Server error', 'FAIL')
    return res
  }
}
const UpdateProductByID = async (val) => {
  try {
    var filler = { _id: val.id}
    var update = await Schema_product.findByIdAndUpdate(filler, val)
    if (update !== null) {
      var res = await response('200', '', 'Update product success', 'SUCCESS')
      return res
    } else {
      var res = await response('200', '', 'Product not found', 'FAIL')
      return res
    }
  } catch (error) {
    var res = await response('500', '', 'Server error', 'FAIL')
    return res
  }
}
module.exports = { Create, GetAllProducts, UpdateProductByID }