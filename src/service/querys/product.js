const { GetAllProducts } = require('../functions/product')
const ProductList = require('../type/product_list')

const GetAllProduct =  {
  type: ProductList,
  resolve: async () => {
    var Result = await GetAllProducts()
    return Result
  }
}

module.exports = { GetAllProduct }