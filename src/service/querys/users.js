const UserList = require('../type/user_list')
const UserObject = require('../type/user_object')
const graphiql = require('graphql')
const { GetAllUsers, GetUserID } = require('../functions/users')
const { Middleware } = require('../functions/checktoken')
const { response } = require('../functions/response')
const { GraphQLString } = graphiql

const GetAllUser =  {
  type: UserList,
  resolve: async () => {
    var Result = await GetAllUsers()
    return Result
  }
}
const GetUserByID = {
  type: UserObject,
  args: {
    token: {type: GraphQLString}
  }, 
  resolve: async (_, args) => {
    var check = await Middleware(args.token)
    if (check !== '') {
      var Result = await GetUserID(check.data._id)
      return Result
    } else {
      var res = await response('500', '', 'Unauthenlize', 'FAIL')
      return res
    }
  }
}

module.exports = { GetAllUser, GetUserByID }
