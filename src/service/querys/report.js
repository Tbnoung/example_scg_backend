const ReportList = require('../type/report_list')
const { GetAllReports } = require('../functions/report')

const GetAllReport =  {
  type: ReportList,
  resolve: async () => {
    var Result = await GetAllReports()
    return Result
  }
}
module.exports = { GetAllReport }