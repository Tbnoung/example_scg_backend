const UserObject = require('../type/user_object')
const { Logins } = require('../functions/login')
const graphiql = require('graphql')

const {
  GraphQLString
} = graphiql

const Login =  {
      type: UserObject,
      args: {
        username: {type: GraphQLString},
        password: {type: GraphQLString}
      },
      resolve: async (_,args) => {
        const LoginUser = await Logins(args)
        return LoginUser
      }
    }

    module.exports = { Login }