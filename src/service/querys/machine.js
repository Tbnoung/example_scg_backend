const graphiql = require('graphql')
const MachineObject = require('../type/machine_object')
const MachineList = require('../type/machine_list')
const { GraphQLString } = graphiql
const { GetAllMachines, GetMachineID, GetMachineByIDMachine } = require('../functions/machine')

const GetAllMachine =  {
  type: MachineList,
  resolve: async () => {
    var Result = await GetAllMachines()
    return Result
  }
}
const GetMachineByID = {
  type: MachineObject,
  args: {
    id: {type: GraphQLString}
  }, 
  resolve: async (_, args) => {
    var Result = await GetMachineID(args)
    return Result
  }
}
const GetMachineByIDUser = {
  type: MachineObject,
  args: {
    id: {type: GraphQLString}
  }, 
  resolve: async (_, args) => {
    var Result = await GetMachineByIDMachine(args)
    return Result
  }
}

module.exports = { GetAllMachine, GetMachineByID, GetMachineByIDUser }