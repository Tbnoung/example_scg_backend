const mongoose = require('mongoose')

var url = "mongodb://localhost:27017/example"
// var url = "mongodb://172.16.113.16:27017/example"

mongoose.connect(url, {useNewUrlParser: true})
mongoose.connection.once('open', () => {
  console.log('Yes! We are connected to mongodb')
})