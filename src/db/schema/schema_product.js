const mongoose = require('mongoose')
const MSchema = mongoose.Schema

const ProductSchema = new MSchema({
  product_name: String,
  price: String,
  qty: String,
  image: String
})

module.exports = mongoose.model('products', ProductSchema)