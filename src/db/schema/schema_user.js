const mongoose = require('mongoose')
const MSchema = mongoose.Schema

const UserSchema = new MSchema({
  username: String,
  password: String,
  firstname: String,
  lastname: String,
  role: String
})

module.exports = mongoose.model('users', UserSchema)