const mongoose = require('mongoose')
const MSchema = mongoose.Schema

const MachineSchema = new MSchema({
  id_user: String,
  username: String,
  location: String,
  discription: String,
  slot: String,
  update_by: String,
  create_by: String,
  email: String,
  machine_product: String
})

module.exports = mongoose.model('machines', MachineSchema)