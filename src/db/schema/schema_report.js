const mongoose = require('mongoose')
const MSchema = mongoose.Schema

const ProductSchema = new MSchema({
  id_machine: String,
  id_product: String,
  location: String,
  discription: String,
  product_name: String,
  qty: String,
  price: String
})

module.exports = mongoose.model('reports', ProductSchema)