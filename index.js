require('dotenv').config()
const port = process.env.PORT
const express = require('express')
const app = express()
const { graphqlHTTP } = require('express-graphql')
const cors = require('cors')
require('./src/db/connect_mongodb')
const schema = require('./src/service/schema/schema')
app.use(cors())
app.use(express.json({limit: '50mb'}));
app.use(express.urlencoded({limit: '50mb'}))
app.use(express.static('src/img'))

app.use('/graphql' , graphqlHTTP({
  graphiql: true,
  schema: schema
}))

app.listen(port, () => {
  console.log('Start Server in Port', port)
})